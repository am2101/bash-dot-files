# ~/.bash_aliases

#-------------
# Safety first
#-------------

# Prevent avoidable file losses
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

#------------
# Convenience
#------------

alias cls='clear'
alias hg='history | grep'
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'

#--------------------
# Tweak list commands
#--------------------

# human readable sizes & color coded files by default
alias ls='ls -1hB --color' 

alias lx='ls -lXB' # Sort by extension
alias lk='ls -lSr' # Sort by size, ascending
alias lt='ls -tr'  # Sort by date, ascending
alias llt='ls -ltr' # Sort by date, ?

# long form: directories first, alphabetic sorting
alias ll='ls -lv --group-directories-first'

alias la='ll -A' # Show hidden files

#------------------------------
# Make directory and cd into it
#------------------------------
function mkdircd () { mkdir -p "$@" && eval cd "\"\$$#\"";
}
