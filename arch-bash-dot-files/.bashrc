#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#---------------------
# Source global bashrc
#---------------------
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

#---------------------
# Import bash settings
#---------------------
if [ -f ~/.bash_settings ]; then
  . ~/.bash_settings
fi

#---------------
# Import aliases
#---------------
if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

#--------------------------------
# Set $DISPLAY if not already set
#--------------------------------
if [ -z ${DISPLAY:=""} ]; then
	echo '$DISPLAY unset'
fi

#------------
# Set $EDITOR
#------------
export EDITOR=vim

#-----------------
# History settings
#-----------------
export HISTCONTROL=ignoredups
export HISTSIZE=1000

#-------------------------------------------
# Set default file and directory permissions
#-------------------------------------------
umask 022

#-------
# Colors
#-------
if [ -f ~/.bash_colors ]; then
  . ~/.bash_colors
fi

#-------------------
# Special characters
#-------------------
if [ -f ~/.bash_chars ]; then
  . ~/.bash_chars
fi

#-------
# Prompt
#-------
if [ -f ~/.bash_prompt ]; then
  . ~/.bash_prompt
fi

#-----
# MOTD
#-----
if [ -f ~/.bash_motd ]; then
  . ~/.bash_motd
fi
